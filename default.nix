with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "hello-world-ml";
  buildInputs = [
    python36Full
    python36Packages.matplotlib
    python36Packages.pyqt4
    agg
    python36Packages.tensorflow 
    python36Packages.Keras
  ];
}
