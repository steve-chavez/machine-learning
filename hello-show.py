import PyQt4
import matplotlib
# For not locking terminal on show
# matplotlib.interactive(True)
matplotlib.use('qt4agg')
import matplotlib.pyplot as plt
plt.plot([1,2,3,4], [1,4,9,16], 'ro')
plt.axis([0, 6, 0, 20])
plt.show()
# plt.plot([1,2,3])
# plt.show()
