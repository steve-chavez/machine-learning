import PyQt4
import matplotlib
matplotlib.use('qt4agg')
import matplotlib.pyplot as plt

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.utils import np_utils

import numpy as np

(x_train, y_train), (x_test, y_test) = mnist.load_data()

batch_size = 128
img_rows, img_cols = 28, 28
nb_classes = 10

x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

y_train = np_utils.to_categorical(y_train, nb_classes)
y_test = np_utils.to_categorical(y_test, nb_classes)

model = Sequential()
model.add(Convolution2D(6, 5, 5, input_shape=(1, img_rows, img_cols), border_mode='same'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), dim_ordering="th"))
model.add(Convolution2D(16, 5, 5, border_mode='same'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2), dim_ordering="th"))
# model.add(Convolution2D(120, 5, 5))
model.add(Convolution2D(120, 5, 5, border_mode='same'))
model.add(Activation('relu'))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(84))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(10))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
nb_epoch = 2

model.fit(x_train, y_train, batch_size = batch_size, nb_epoch = nb_epoch, verbose = 1, validation_data = (x_test, y_test))

score = model.evaluate(x_test, y_test, verbose = 0)

print('Test score:', score[0])
print('Test accuracy:', score[1])

res = model.predict_classes(x_test[:9])
plt.figure(figsize=(10, 10))

for i in range(9):
  plt.subplot(3, 3, i + 1)
  plt.imshow(x_test[i, 0], cmap='gray')
  plt.gca().get_xaxis().set_ticks([])
  plt.gca().get_yaxis().set_ticks([])
  plt.ylabel('prediction = %d' % res[i], fontsize = 18)

plt.show()
