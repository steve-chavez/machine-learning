import PyQt4
import matplotlib
matplotlib.use('qt4agg')
import matplotlib.pyplot as plt

from keras.datasets import mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()

for i in range(9):
  plt.subplot(3, 3, i+1)
  plt.tight_layout()
  plt.imshow(x_train[i], cmap='gray', interpolation='none')
  plt.title("Digit: {}".format(y_train[i]))
  plt.axis('off')
  # Another way
  # plt.xticks([])
  # plt.yticks([])

plt.show()

# img_rows, img_cols = 28, 28

# x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)

# for i in range(9):
  # plt.subplot(3, 3, i + 1)
  # plt.imshow(x_train[i, 0], cmap = 'gray')
  # plt.axis('off')

# plt.show()
